;; =========== Install packages =========
(require 'package)
(add-to-list 'package-archives
	     '("melpa" . "http://stable.melpa.org/packages/") t)
(package-initialize)

(defvar myPackages
  '(better-defaults
    elpy
    py-autopep8
    material-theme))

(mapc #'(lambda (package)
    (unless (package-installed-p package)
      (package-install package)))
      myPackages)

(load-theme 'material t) ;; load material theme
(elpy-enable) ; enable elpy
;; (require 'py-autopep8) ; install autopep8 to get this to work
;; (add-hook 'elpy-mode-hook 'py-autopep8-enable-on-save)
(add-hook 'python-mode-hook (lambda () (flycheck-mode 1))) ; flycheck for py
(add-hook 'js2-mode-hook (lambda () (flycheck-mode 1))) ; flycheck for js2
(add-hook 'latex-mode-hook (lambda () (toggle-word-wrap 1))) ; word wrap latex

;; =========== Macros

(fset 'breakpoint
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ("imoport pdb; pdb.set_trace();" 0 "%d")) arg)))
(global-set-key (kbd "M-p") 'breakpoint)

;; =========== I've forgotten what these do, but they are probably awesome =========

(setq x-select-enable-clipboard t)
(setq show-paren-delay 0)
(show-paren-mode 1)

(setq inhibit-startup-message t) ;; hide the startup message
;(global-linum-mode t) ;; enable line numbers globally

 ;; ========== Highlight all characters beyond 80 for each line ==========
(require 'whitespace)
(setq whitespace-style '(face empty tabs lines-tail trailing))
(global-whitespace-mode t)
 ;; ========== Enable Line and Column Numbering ==========
(line-number-mode 1)
(column-number-mode 1)
 ;; ========== use Shift+arrow_keys to move cursor around split panes ==========
(windmove-default-keybindings)
 ;; ========== M-; toggles commenting of current line
 (defun comment-dwim-line (&optional arg)
  "Replacement for the comment-dwim command.
   If no region is selected and current line is not blank and
   we are not at the end of the line,
   then comment current line.
   Replaces default behaviour of comment-dwim,
   when it inserts comment at the end of the line."
  (interactive "*P")
  (comment-normalize-vars)
  (if (and (not (region-active-p)) (not (looking-at "[ \t]*$")))
      (comment-or-uncomment-region (line-beginning-position) (line-end-position))
    (comment-dwim arg)))
(global-set-key "\M-;" 'comment-dwim-line)

 ;; ========== M-; toggles commenting of current line
  (defun quick-copy-line ()
  "Copy the whole line that point is on and move to the beginning of the next line.
    Consecutive calls to this command append each line to the
    kill-ring."
  (interactive)
  (let ((beg (line-beginning-position 1))
	(end (line-beginning-position 2)))
    (if (eq last-command 'quick-copy-line)
	(kill-append (buffer-substring beg end) (< end beg))
      (kill-new (buffer-substring beg end))))
)
(global-set-key "\C-c\C-k" 'quick-copy-line)

 ;; ========== Disable \ character in line wrapping
(set-display-table-slot standard-display-table 'wrap ?\ )
(put 'downcase-region 'disabled nil)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages (quote (material-theme py-autopep8 elpy better-defaults))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
