# .bashrc

# set -x

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

######################
### General settings
######################
alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'

# Set an unlimited stack size
ulimit -s unlimited

# Set default rwx-permissions for new files
umask 0002

# Increase bash_history length
export HISTSIZE=
export HISTFILESIZE=
# Append to current historyfile instead of overwriting
shopt -s histappend

export EDITOR='emacs -nw'

case "$TERM" in
    xterm*|rxvt*)
        # Ctrl+up/down, pgup/pgdw does smart search in command history
        bind '"\e[1;5A":history-search-backward'
        bind '"\e[1;5B":history-search-forward'
        bind '"\e[5~": history-search-backward'
        bind '"\e[6~": history-search-forward'

        # Ctrl-right/left jumps entire words
        bind '"\e[1;5C": forward-word'
        bind '"\e[1;5D": backward-word'

        # Shift+tab, Shift+½ does menu complete
        bind '"\e[Z": menu-complete'
        bind '"½": menu-complete-backward'
esac

# Export pass selection to middle-click
export PASSWORD_STORE_X_SELECTION=primary

# Ctrl-delete kills word forward
bind '"\e[3;5~": kill-word'
# Ctrl-backspace kills word backward
bind '"\C-h": backward-kill-word'

######################
### General aliases
######################
alias du1='du -h --max-depth=1'
alias dumost='du -hsx * | sort -rh | head -10'
alias emacs='emacs -nw'
alias ls='ls --color=auto'
alias xopen='xdg-open 2>/dev/null'
alias bashrc='emacs ~/.bashrc && source ~/.bashrc'
alias passc='pass -c'

sourceve(){ source ~/virtualenvs/$1/bin/activate; }
sourcedev(){ source ~/dev/$1/bin/activate; }
clearer(){ for i in {1..60}; do echo -ne '\n'; done; clear;}
define(){ gnome-dictionary --look-up $1 > /dev/null 2>&1 &}
logcheck(){
    grep -aEi$2 'sigsegv|warning|error|missing' $1 | grep -av 'missing mass'| grep -va 'WARNING BOUNDARY VALUES ASSUMED TO BE IN MOL/MOL' | less -F
}
pwdf()
{
    if [ -z "$1" ]; then
        pwdf=$(readlink -f ".")
    else
        pwdf=$(readlink -f "$1")
    fi
    pwdf=${pwdf// /\\ }
    echo $pwdf | tr -d '\n' | xclip
    echo $pwdf
}
printdm(){ echo "https://dmserver.smhi.se/CyberDOCS/quickstart.asp?show=VIEW:$1" ;}
printmyinfo()
{
    echo -e 'Address:\nSMHI\nFolkborgsvägen 17\n601 76 Norrköping\nSweden\n'
    echo -e 'Phone:\n011-495 84 32'
}
win2unix(){
    dir="$1"
    dir=$(echo "$dir" | sed -e 's:\\winfs\-proj:/data:' -e 's:\\:\/:g')
    echo $dir | tr -d '\n' | xclip
    echo $dir
}
unix2win(){
    if [ -z "$1" ]; then
        pwdf=$(readlink -f ".")
    else
        pwdf=$(readlink -f "$1")
    fi
    pwdf=${pwdf// /\\ }
    pwdf=$(echo "$pwdf" | sed -e 's:\/:\\:g' -e 's:\\ : :g' -e 's:proj[0-9]:proj:g' -e 's:^\\data:\\\\winfs-proj:g')
    echo $pwdf | tr -d '\n' | xclip
    echo $pwdf
}


######################
### c21746 specific settings
######################
if [ $HOSTNAME = "c21746.ad.smhi.se" ]; then
    # Paths
    export PATH=$PATH:~/bin/
    export PATH=$PATH:/data/proj/MATCH/local/bin/
    export PATH=$PATH:~/.local/npm/bin
    export PATH=$PATH:/home/a002004/local/miniconda2/bin

    export PYTHONPATH=/data/proj/MATCH/local/pylib/
    export TABLE_PATH=/data/proj/MATCH/local/dbase/

    # # Limiting touchpad size while waiting for a Palm Detection solution
    # synclient AreaTopEdge=1612 AreaRightEdge=5167 AreaLeftEdge=1861

    # Turn Bluetooth off automatically
    rfkill block bluetooth

    # Set default printer
    export PRINTER=printer-24-2nps
fi


######################
### bi specific settings
######################
if [ $HOSTNAME = "bi1.nsc.liu.se" ]; then
    ### Various settings
    export TABLE_PATH=/home/sm_foul/metgraf-bi/dbase/
    export CVSROOT=/home/sm_foul/repos/cvs
    export LIB_PATH=/home/sm_foul/local

    # Set the path for a working coast path for jmaps/metgraf
    export GMT_COAST_PATH=/home/sm_foul/local/share/coast

    LD_LIBRARY_PATH=/opt/scali/lib:$LD_LIBRARY_PATH
    LD_LIBRARY_PATH=/software/intel/composer_xe_2011_sp1.10.319/compiler/lib/intel64/:/home/sm_lrobe/local/lib:$LD_LIBRARY_PATH
    LD_LIBRARY_PATH=/software/intel/fce/10.1.011/lib:/software/apps/python/2.7.3/smhi-1/lib:$LD_LIBRARY_PATH
    export LD_LIBRARY_PATH
    PYTHONPATH=/home/sm_foul/metgraf-bi/pylib/:$PYTHONPATH
    PYTHONPATH=/home/sm_marto/pybib/:$PYTHONPATH
    export PYTHONPATH

    PATH=/usr/local/mpich/man:$PATH
    PATH=/usr/local/bin:$PATH
    PATH=/local/bin:$PATH
    PATH=/home/sm_foul/metgraf-bi/bin/:$PATH
    export PATH

    # module load emacs/24.4-gcc447
    module load buildenv-intel/2015-1
    module load ncview
    module load netcdf/4.3.2-i1402-hdf5-1.8.14
    module load cdo
    module load gdal
    module load geos
    #	module load mars/3.0

    module load intel/15.0.1.133
    module load impi/5.0.2.044
    # module load python/2.7.13-anaconda-5.0.0.1
    module load python3/3.6.5-anaconda-5.2.0
    module load netcdf/4.3.2-i1402-hdf5-1.8.14
    module load hdf5/1.8.14-i1501 hdfview/2.11.0
fi


######################
### c21746 specific aliases
######################
if [ $HOSTNAME = "c21746.ad.smhi.se" ]; then
    # Anvandbara ssh-alias
    alias aniara='ssh -AX aniara'
    alias atlantis='ssh -AX atlantis'
    alias bi='ssh -AX bi'
    alias cylix='ssh cylix'
    alias ecmwf='ssh -l smda -X ecaccess.ecmwf.int'
    alias elin4='ssh -AX a002004@elin4-utv.smhi.se'
    alias estmdb='ssh -AX estmdb'
    alias frost='ssh -AX frost'
    alias ufrost='ssh -AX fwindmark@frost'
    alias krypton='ssh -AX krypton'
    alias hamberg='ssh -AX hamberg'
    alias uhamberg='ssh -AX fwindmark@hamberg'
    alias schaffer='ssh -AX schaffer'
    alias uschaffer='ssh -AX fwindmark@schaffer'
    alias monin='ssh -AX monin'
    alias umonin='ssh -AX fwindmark@monin'
    alias nina='ssh -AX nina'
    alias unina='ssh -AX fwindmark@nina'


    # Useful directories
    alias cdAkonsult='cd /data/proj/A-konsult/'
    alias cdAkonsult_arkiv='cd /data/lang/A-konsult_arkiv/'
    alias cdAuverktyg='cd /data/proj/A-konsult/Lopande_verksamhet/Au-verktyg'
    alias cdBosnienDH='cd /data/proj/A-konsult/Bosnien_datainsammling_fas2/'
    alias cdBosnienSAS='cd /data/proj9/A-konsult/Bosnien_SAS_2019/'
    alias cdBAPStest='cd /data/dmzshared/prodtest/baps/'
    alias cdBAPSprod='cd /data/dmzshared/24/baps/'
    alias cdBasar='cd /data/proj/Luftkvalitet/SIMAIR/Basår/Basår_2019/'
    alias cdCAMS='cd /data/proj/A-konsult/CAMS50'
    alias cdGeo='cd /data/proj/A-konsult/Lopande_verksamhet/Apl/SMED_Luft/Geofordelning/'
    alias cdEnergimyndigheten2016='cd /data/proj/A-konsult/Energimyndigheten_Shipair_Bränslestatistik_2016_1786_9.5'
    alias cdEnergimyndigheten2018='cd /data/proj/A-konsult/Energimyndigheten_Shipair_Bränslestatistik_2017-2018_2018_2030_9.5/'
    alias cdHome='cd /smhi/home/a002004/'
    alias cdLopande='cd /data/proj/A-konsult/Lopande_verksamhet/'
    alias cdFredrikWindmark='cd /data/proj/A-konsult/Lopande_verksamhet/Apl/users/FredrikWindmark/'
    alias cdLuftkvalitet='cd /data/proj/Luftkvalitet/'
    alias cdLuftkvalitet_arkiv='cd /data/lang/Luftkvalitet_arkiv/'
    alias cdLuftmiljosegmentet=' cd /data/proj/A-konsult/Lopande_verksamhet/Luftmiljösegmentet'
    alias cdMATCH='cd /data/proj/MATCH'
    alias cdMATCHkurs='cd /data/proj/MATCH/admin/kurs/MATCHkurs2017'
    alias cdMiljoovervakning='cd /data/proj/A-konsult/Miljoovervakning'
    alias cdMiljoovervakning_arkiv='cd /data/lang/miljoovervakningen/'
    alias cdOfferter='cd /data/proj/A-konsult/Lopande_verksamhet/Luftmiljösegmentet/Luft\ Sverige\ marknad/förfrågningar/2019'
    alias cdProjects='cd ~/Documents/projects/'
    alias cdPapers='cd ~/Documents/papers/'
    alias cdPicsAll='cd /data/prodkap/BildkatalogMP'
    alias cdPicsLuft='cd /data/proj/Luftkvalitet/SIMAIR/Imagebilder/'
    alias cdShipair='cd /data/proj/A-konsult/Lopande_verksamhet/Apl/Shipair'
    alias cdTemp='cd /data/temp/a002004/'
    alias cdUppdrag='cd /data/proj/Uppdrag/'
    alias cdSpecifikationer='cd /data/proj/A-konsult/Lopande_verksamhet/Luftmiljösegmentet/Uppdrag/offerter,\ interna\ beställningar\ m.m/förfrågningar/'
    alias cdTrafa='cd /data/proj9/A-konsult/Shipair_Trafa_bransleforbrukning'
    alias cdValidering='cd /data/proj/A-konsult/Validering_av_SIMAIR_för_basår_2014-2015_2016-1374-2.4/'
    alias cdVTI='cd /data/proj/A-konsult/Shipair_VTI_Svenska_fartygsflottan_2019_798_9.5/'
    alias cdWeb='cd /data/proj/A-konsult/Lopande_verksamhet/Luftmiljösegmentet/Hemsidan'
    alias cdWinproj='cd /smhi/home/a002004/Fredrik/projects'

    # Misc alias
    alias pg96_start='$HOME/.local/postgresql-9.6.3/bin/pg_ctl start -o "-c unix_socket_directories=$XDG_RUNTIME_DIR" -D $HOME/local/pgsql-9.6.3/data -w -t 300'
    alias pg96_stop='$HOME/.local/postgresql-9.6.3/bin/pg_ctl stop -D $HOME/local/pgsql-9.6.3/data -m fast'
    alias docker="sudo smhi-docker"
    alias docker-compose="sudo docker-compose"
    cdreports(){ cd ~/Documents/reports/$1 ;}
    cdw(){ cd "$(echo "$1" | sed "s@\\\\winfs-proj@data@" | tr '\\' '/')" ;}
fi


######################
### bi specific aliases
######################
if [ $HOSTNAME = "bi1.nsc.liu.se" ]; then
    ### Directories
    alias cdCAMS="cd /nobackup/smhid11/sm_frewi/macc/re-ana/work/cams_2015_reana/"
    alias cdCAMS2014="cd /nobackup/smhid12/sm_helal/macc/re-ana/work/cams_2014_reana/"
    alias cdKallfordelning="cd /nobackup/smhid13/luftkval/sm_marto_d11/simair/match_151109/testcases/Urban_KF_2015"
    alias cdBUM="cd /home/luftkval/basar/bum/match_160506/testcases/Urban_SMED_2016"
    alias cdBUMres="cd /nobackup/smhid13/luftkval/basar/bum/2016/RESULTS_SMED_2016"
    alias cdMATCHkurs="cd /nobackup/smhid11/sm_foul/match_course/sm_frewi"

    ### Aliases
    alias sqf='squeue -usm_frewi -o "%.7i %.15j %.8u %.8T %.10M %.9l %10a %.4D %R"'
    alias sq='squeue -o "%.7i %.15j %.8u %.8T %.10M %.9l %10a %.4D %R"'
    alias sqa='squeue -o "%.7i %.15j %.8u %.8T %.10M %.9l %10a %.4D %R" | grep -e " sm_a " -e "_fat "'

    cdf()
    {
        if [ -z "$1" ]; then
            cd /home/sm_frewi
        else
            cd /nobackup/smhid${1}/sm_frewi/$2
        fi
    }
    cdl()
    {
        if [ -z "$1" ]; then
            cd /home/luftkval
        else
            cd /nobackup/smhid${1}/luftkval/$2
        fi
    }
    int()
    {
        node=`sqf |grep _interactive |awk '{print $8}'`
        if [ `sqf |grep -c _interactive` -eq 0 ]; then
           echo "Starting an interactive session"
           export FIRST=True
           interactive -C fat -n1 -t 180
        else
            export FIRST=False
            ssh -X $node
        fi
    }
fi
if [[ $HOSTNAME = "nina" || $HOSTNAME = "frost" || $HOSTNAME = "monin" ]]; then
    GULLBRITT=265573140
    GULLMAJ=265573150
    PERNILLE=219014974
    STENA_DANICA=265177000
    STENA_SAGA=265001000
    VISBY=265865000
    export GULLBRITT GULLMAJ PERNILLE STENA_DANICA STENA_SAGA VISBY

    cdbp () {
        cd $DBAS_PATH/$1;
    }
    dbp () {
        export AVDBNAME=$1;
        export DBAS_PATH=/usr/airviro/data/$AVDBNAME/;
    }
fi

# added by Miniconda2 installer

# set +x
